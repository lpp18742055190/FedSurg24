import csv
import os.path

from metrics4MICCAI24.dice_score import get_f1
from metrics4MICCAI24.expected_cost import get_expected_cost
from metrics4MICCAI24.utils import get_final_csv


def task_1():
    """
    Generalization Ability of the Server Model
    """
    print(f'###### center 5 - global generalization ability #####')
    merged_results = get_final_csv('/app/predictions/task_1/pred.csv', '/app/testset/center_4/data.csv')

    f1_metric = get_f1(merged_results, num_classes=10)
    print(f'F1: {f1_metric}')

    expected_cost = get_expected_cost(merged_results, num_classes=10)
    print(f'expected_cost: {expected_cost}')

    data = [['f1', 'expected_cost'], [f1_metric, expected_cost]]
    with open('/app/results/task_1_metrics.csv', 'w') as csvfile:
        csvwriter = csv.writer(csvfile)
        csvwriter.writerows(data)


def task_2():
    """
    Adaptation Ability of the models of each center
    """
    for center in range(4):
        print(f'###### center {center} - local adaptation ability #####')
        merged_results = get_final_csv(f'/app/predictions/task_2/center_{center}_pred.csv',
                                       f'/app/testset/center_{center}/data.csv')

        f1_metric = get_f1(merged_results, num_classes=10)
        print(f'F1: {f1_metric}')

        expected_cost = get_expected_cost(merged_results, num_classes=10)
        print(f'expected_cost: {expected_cost}')

        data = [['f1', 'expected_cost'], [f1_metric, expected_cost]]
        with open(f'/app/results/task_2_center{center}_metrics.csv', 'w') as csvfile:
            csvwriter = csv.writer(csvfile)
            csvwriter.writerows(data)

    print(f'###############################')


if __name__ == '__main__':
    task_1()
    task_2()

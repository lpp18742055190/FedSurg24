import csv
import numpy as np
import torch
from torch import nn
import torch.nn.functional as F
from torch.utils.data import DataLoader
from torchvision.datasets import CIFAR10
import torchvision.transforms as transforms

DEVICE = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


class Net(nn.Module):
    def __init__(self):
        super().__init__()
        self.conv1 = nn.Conv2d(3, 6, 5)
        self.pool = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(6, 16, 5)
        self.fc1 = nn.Linear(16 * 5 * 5, 120)
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, 10)

    def forward(self, x):
        x = self.pool(F.relu(self.conv1(x)))
        x = self.pool(F.relu(self.conv2(x)))
        x = x.view(-1, 16 * 5 * 5)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        return self.fc3(x)


def load_model(model, model_path, is_state_dict=False):
    if is_state_dict:
        model.load_state_dict(torch.load(model_path))
    else:
        loaded_arrays = np.load(model_path)
        weights = [loaded_arrays[key] for key in loaded_arrays.keys()]
        with torch.no_grad():
            for param, weight in zip(model.parameters(), weights):
                param.copy_(torch.tensor(weight))
    return model


def get_predictions(model, testloader):
    predictions = [['item_id', 'prediction']]
    with torch.no_grad():
        for i, data in enumerate(testloader):
            images = data[0].to(DEVICE)
            outputs = model(images)
            _, predicted = torch.max(outputs.data, 1)
            predictions.append([i, predicted.item()])
    return predictions


def save_list_to_csv(data_list, file_path):
    with open(file_path, 'w', newline='') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerows(data_list)


def get_labels(testloader):
    labels = []
    for i, data in enumerate(testloader):
        labels.append([i, data[1].item()])
    save_list_to_csv(labels, "/app/output/predictions/data.csv")


def main():
    transform = transforms.Compose([transforms.ToTensor(), transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])
    model = Net().to(DEVICE).eval()

    # Task 1
    testset = CIFAR10("/app/input/center_4/", train=False, download=False, transform=transform)
    testloader = DataLoader(testset, batch_size=1)
    model = load_model(model, "/app/models/task_1/model.npz")
    predictions = get_predictions(model, testloader)
    save_list_to_csv(predictions, "/app/output/predictions/task_1/pred.csv")

    # Task 2
    for center in range(4):
        testset = CIFAR10(f"/app/input/center_{center}/", train=False, download=False, transform=transform)
        testloader = DataLoader(testset, batch_size=1)
        model = load_model(model, f"/app/models/task_2/best_model_center_{center}.pth", is_state_dict=True)
        predictions = get_predictions(model, testloader)
        save_list_to_csv(predictions, f"/app/output/predictions/task_2/center_{center}_pred.csv")

    get_labels(testloader)


if __name__ == "__main__":
    main()

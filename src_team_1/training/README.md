## Dataset for Demo
The dataset used for the demo is the [CIFAR10](https://www.cs.toronto.edu/~kriz/cifar.html) dataset.
Direct Downloadlink can be found [here](https://www.cs.toronto.edu/~kriz/cifar-10-python.tar.gz).
The dataset is downloaded and stored in the `data\clientX` folders.

## Usage
```bash
docker build -t fl_challenge:team_name .
```

Start federated learning 
```bash
docker-compose up .
```

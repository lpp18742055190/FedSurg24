from logging import INFO
from typing import List, Tuple, Union, Optional, Dict
import numpy as np
import flwr as fl
import torch
from flwr.common import FitRes, Scalar, Parameters
from flwr.server.client_proxy import ClientProxy
from flwr.common.logger import log
from flwr.server.strategy import FedAvg
from torch import nn
import torch.nn.functional as F


class Net(nn.Module):
    def __init__(self) -> None:
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(3, 6, 5)
        self.pool = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(6, 16, 5)
        self.fc1 = nn.Linear(16 * 5 * 5, 120)
        self.fc2 = nn.Linear(120, 84)
        self.fc3 = nn.Linear(84, 10)

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        x = self.pool(F.relu(self.conv1(x)))
        x = self.pool(F.relu(self.conv2(x)))
        x = x.view(-1, 16 * 5 * 5)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x


class SaveModelStrategy(FedAvg):
    def __init__(self):
        super().__init__()
        self.model = Net()

    def aggregate_fit(
            self,
            server_round: int,
            results: List[Tuple[fl.server.client_proxy.ClientProxy, fl.common.FitRes]],
            failures: List[Union[Tuple[ClientProxy, FitRes], BaseException]],
    ) -> Tuple[Optional[Parameters], Dict[str, Scalar]]:
        # Call aggregate_fit from base class (FedAvg) to aggregate parameters and metrics
        aggregated_parameters, aggregated_metrics = super().aggregate_fit(server_round, results, failures)

        if aggregated_parameters is not None:
            # Convert `Parameters` to `List[np.ndarray]`
            aggregated_ndarrays: List[np.ndarray] = fl.common.parameters_to_ndarrays(aggregated_parameters)

            # Save aggregated_ndarrays
            log(INFO, f"Saving round {server_round} aggregated_ndarrays...")
            np.savez(f"/app/output/round-{server_round}-weights.npz", *aggregated_ndarrays)
        return aggregated_parameters, aggregated_metrics


if __name__ == "__main__":
    log(INFO, f"Flower version: {fl.__version__}")
    strategy = SaveModelStrategy()
    fl.server.start_server(config=fl.server.ServerConfig(num_rounds=10), strategy=strategy)

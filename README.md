# FedSurg24
This repository contains an example implementation of the Federated Learning for Surgical Vision Challenge.
This example is built on top of our [Docker for Challenges](https://gitlab.com/nct_tso_public/challenge-docker) and the minimal [CIFAR10 example from FLOWER](https://flower.ai/docs/framework/tutorial-quickstart-pytorch.html).
For our challenge, we will provide you with the data (see useful information) and the evaluation script (see src_team1/evaluation).

Furthermore, this repository contains the evaluation script for the challenge "Federated Learning for Surgical Vision".

The challenge is part of the [MICCAI 2024 Endoscopic Vision Challenge](http://endovis.org/).

## Tasks:
1. **Task 1**: Generalization Ability of the Algorithm.

The algorithm should perform well on the data of a client which is not part of the training set.
The aim is to test the generalization ability of the algorithm.
The algorithm should store the global model so that the generalization ability can be tested.

Input -> string of folder path where images are located

Output -> global model

2. **Task 2**: Adaptation Ability of the Algorithm.

The provided algorithm should perform well on test data of each single client independently.
This allows us to evaluate the adaptation ability of the algorithm.
The algorithm should store for each center a model so that the adaptation ability can be tested.

Input -> string of folder path where images are located

Output -> local model for each center

## Getting Started
The submission of the algorithm will be done by Docker containers.
In contrast to other challenges, you will provide us the training and test script inside the Docker container.
Training and testing of the model based on the complete training or test set will be performed on our cluster.


## Folder Structure
The repository is structured as follows:
- `src_team_1`: Contains the source code of example team 1 based on Example Cifar10.
  - `training`: Contains the training code.
    -  `output`: Contains the output of the training. (e.g. model weights)
    - `README.md`: Contains the description of the training code.
    - `requirements.txt`: Contains the required libraries for the training code.
    - `src`: Contains the source code of the training.
    - `Dockerfile`: Contains the Dockerfile for the training code.
    - `docker-compose.yml`: Contains the Docker Compose file for the training code. 
  - `testing`: Contains the source code of the testing.
    - `output`: Contains the output of the testing, i.e. predictions.csv where the image_id and the predicted class are stored.
    - `README.md`: Contains the description of the testing code.
    - `requirements.txt`: Contains the required libraries for the testing code.
    - `src`: Contains the source code of the testing.
    - `Dockerfile`: Contains the Dockerfile for the testing code.
    - `docker-compose.yml`: Contains the Docker Compose file for the testing code.
  - `evaluation`: Contains the source code of the evaluation. (Provided by us)
    - `../data/results/team1/`: Contains the output of the evaluation. (e.g. task_1_metrics.csv) 
    - `README.md`: Contains the description of the evaluation code.
    - `requirements.txt`: Contains the required libraries for the evaluation code.
    - `src`: Contains the source code of the evaluation.
    - `Dockerfile`: Contains the Dockerfile for the evaluation code.
    - `docker-compose.yml`: Contains the Docker Compose file for the evaluation code.

## Useful Information:
- The trainset and testset are stored in a seperate `data` folder.
  - It is mounted to the Docker container. 
  - The exact path will be set by us.
  - Same paths will be used for all teams.
- The gt.csv file is stored in a seperate folder.
  - It is not mounted to the Docker container.
  - Same paths will be used for all teams.
  - The gt.csv path will be set by us in the evaluation script.
- The data structure will look like that:
  
```
- data
|--- train
|    |--- client1
|    |    |--- data.csv (image_id, class)
|    |    └--- frames
|    |         |--- frame1.jpg
|    |         |--- frame2.jpg
|    |         └--- ...
|    |--- client2
|    |    |--- data.csv
|    |    └--- frames
|    |         |--- frame1.jpg
|    |         |--- frame2.jpg
|    |         └--- ...
|    └--- ...
|--- test
|    |--- client1
|    |    └--- frames
|    |         |--- frame1.jpg
|    |         |--- frame2.jpg
|    |         └--- ...
|    |--- client2
|    |    └--- frames
|    |         |--- frame1.jpg
|    |         |--- frame2.jpg
|    |         └--- ...
|    └--- ...
```

- For training, you will have data from four different centers.
- For testing, you will have testing data from the four centeres (task 2) and additionally from a fifth center (task 1).
- The evaluation script is provided by us.
